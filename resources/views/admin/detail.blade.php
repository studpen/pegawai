<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Dashboard | Tambah Pegawai</title>
  <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" />
  {{-- icon website --}}
  <link rel="icon" href="{{ asset('assets/img/kaiadmin/favicon.ico') }}" type="image/x-icon" />

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/plugins.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/kaiadmin.min.css') }}" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />
</head>

<body>
  <div class="wrapper">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- End Sidebar -->

    <div class="main-panel">
      {{-- Header --}}
      @include('admin.template.header')
      {{-- End Header --}}

      {{-- Main Content --}}
      <div class="container">
        <div class="page-inner">
          <div class="page-header">
            <h3 class="fw-bold mb-3">Detail Pegawai</h3>
            <ul class="breadcrumbs mb-3">
              <li class="nav-home">
                <a href="{{route('dashboard')}}">
                  <i class="icon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="icon-arrow-right"></i>
              </li>
              <li class="nav-item">
                <a href="#">Detail Pegawai</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                    <h3 class="fw-bold">Profil Pegawai</h3>
                    <img src="{{ asset('assets/img/upload/'.$pegawai->foto) }}" class="img-thumbnail my-4" alt="..." width="210" height="330">
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">NIK</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->NIK}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">NPWP</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->NPWP}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Nama</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->nama}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Jabatan</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->nama_jabatan}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Tanggal Lahir</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->tanggal_lahir}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Jenis Kelamin</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->jenis_kelamin}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Alamat</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->alamat}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">No Telepon</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->no_telepon}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Email</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->email}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Tanggal Bergabung</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->tanggal_bergabung}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Gaji</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{ 'Rp '. number_format($pegawai->gaji, 0, ',', '.') }}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Status Pegawai</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->status_pegawai}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Pendidikan Terakhir</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->pendidikan_terakhir}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Nama Instuisi</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->nama_instuisi}}</p>
                    </div>
                    <div class="row ms-2 g-0">
                        <p class="m-0 p-0 col-2 fw-semibold">Tahun Lulus</p>
                        {{-- <p class="m-0 p-0 col-1">:</p> --}}
                        <p class="m-0 p-0 col-10">: {{$pegawai->tahun_lulus}}</p>
                    </div>
                    <a href="{{route('dashboard')}}" class="btn btn-secondary mt-3">Kembali</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery-3.7.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>

  <!-- jQuery Scrollbar -->
  <script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
  <!-- Kaiadmin JS -->
  <script src="{{ asset('assets/js/kaiadmin.min.js') }}"></script>
  <!-- Kaiadmin DEMO methods, don't include it in your project! -->
  <script src="{{ asset('assets/js/setting-demo2.js') }}"></script>
  <!-- Fonts and icons -->
  <script src="{{ asset('assets/js/plugin/webfont/webfont.min.js') }}"></script>
  <script>
    WebFont.load({
      google: { families: ["Public Sans:300,400,500,600,700"] },
      custom: {
        families: [
          "Font Awesome 5 Solid",
          "Font Awesome 5 Regular",
          "Font Awesome 5 Brands",
          "simple-line-icons",
        ],
        urls: ["{{ asset('assets/css/fonts.min.css') }}"],
      },
      active: function () {
        sessionStorage.fonts = true;
      },
    });
  </script>
</body>

</html>