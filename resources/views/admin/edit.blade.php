<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Dashboard | Edit Pegawai</title>
  <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" />
  {{-- icon website --}}
  <link rel="icon" href="{{ asset('assets/img/kaiadmin/favicon.ico') }}" type="image/x-icon" />

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/plugins.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/kaiadmin.min.css') }}" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />
</head>

<body>
  <div class="wrapper">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- End Sidebar -->

    <div class="main-panel">
      {{-- Header --}}
      @include('admin.template.header')
      {{-- End Header --}}
      
      {{-- Main Content --}}
      <div class="container">
        <div class="page-inner">
          <div class="page-header">
            <h3 class="fw-bold mb-3">Form Edit Pegawai</h3>
            <ul class="breadcrumbs mb-3">
              <li class="nav-home">
                <a href="{{route('dashboard')}}">
                  <i class="icon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="icon-arrow-right"></i>
              </li>
              <li class="nav-item">
                <a href="#">Edit Pegawai</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                {{-- Form Tambah Pegawai --}}
                <div class="card-body">
                  <form class="row g-3" action="edit_data" method="post" enctype="multipart/form-data">
                    @csrf
                    {{-- Baris 1 --}}
                    <div class="col-md-6">
                      <label for="NIK" class="form-label fw-semibold">NIK</label>
                      <input type="text" class="form-control" id="NIK" name="NIK" value="{{$pegawai->NIK}}" required>
                    </div>
                    <div class="col-md-6">
                      <label for="NPWP" class="form-label fw-semibold">NPWP</label>
                      <input type="text" class="form-control" id="NPWP" name="NPWP" value="{{$pegawai->NPWP}}" required>
                    </div>
                    {{-- Baris 2 --}}
                    <div class="col-md-4">
                      <label for="nama" class="form-label fw-semibold">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" value="{{$pegawai->nama}}" required>
                    </div>
                    <div class="col-md-4">
                      <label for="tanggal_lahir" class="form-label fw-semibold">Tanggal lahir</label>
                      <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{$pegawai->tanggal_lahir}}" required>
                    </div>
                    <div class="col-md-4">
                      <label for="jenis_kelamin" class="form-label d-block">Jenis Kelamin</label>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="Laki-laki" 
                          {{ $pegawai->jenis_kelamin == 'Laki-laki' ? 'checked' : '' }}>
                        <label class="form-check-label" for="inlineRadio1">Laki-laki</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="Perempuan" 
                          {{ $pegawai->jenis_kelamin == 'Perempuan' ? 'checked' : '' }}>
                        <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                      </div>
                    </div>
                    {{-- Baris 3 --}}
                    <div class="col-md-12">
                      <label for="alamat" class="form-label fw-semibold">Alamat</label>
                      <input type="text" class="form-control" id="alamat" name="alamat" value="{{$pegawai->alamat}}" required>
                    </div>
                    {{-- Baris 4 --}}
                    <div class="col-md-4">
                      <label for="email" class="form-label fw-semibold">Email</label>
                      <input type="email" class="form-control" id="email" name="email" value="{{$pegawai->email}}" required>
                    </div>
                    <div class="col-md-4">
                      <label for="no_telepon" class="form-label fw-semibold">No Telepon</label>
                      <input type="text" class="form-control" id="no_telepon" name="no_telepon" value="{{$pegawai->no_telepon}}" required>
                    </div>
                    <div class="col-md4">
                      <label for="nama_jabatan" class="form-label fw-semibold">Nama Jabatan</label>
                      <select class="form-control" id="nama_jabatan" name="nama_jabatan" required>
                        <option value="">Pilih Jabatan</option>
                        <option value="General Manager" {{ $pegawai->nama_jabatan == 'General Manager' ? 'selected' : '' }}>General Manager</option>
                        <option value="Plant Manager" {{ $pegawai->nama_jabatan == 'Plant Manager' ? 'selected' : '' }}>Plant Manager</option>
                        <option value="Supervisor Produksi" {{ $pegawai->nama_jabatan == 'Supervisor Produksi' ? 'selected' : '' }}>Supervisor Produksi</option>
                        <option value="Operator Mesin" {{ $pegawai->nama_jabatan == 'Operator Mesin' ? 'selected' : '' }}>Operator Mesin</option>
                        <option value="Teknisi Mesin" {{ $pegawai->nama_jabatan == 'Teknisi Mesin' ? 'selected' : '' }}>Teknisi Mesin</option>
                        <option value="Foreman" {{ $pegawai->nama_jabatan == 'Foreman' ? 'selected' : '' }}>Foreman</option>
                      </select>
                    </div>
                    {{-- Baris 5 --}}
                    <div class="col-md-3">
                      <label for="tanggal_bergabung" class="form-label fw-semibold">Tanggal Bergabung</label>
                      <input type="date" class="form-control" id="tanggal_bergabung" name="tanggal_bergabung" value="{{$pegawai->tanggal_bergabung}}" required>
                    </div>
                    <div class="col-md-3">
                      <label for="pendidikan_terakhir" class="form-label fw-semibold">Pendidikan Terakhir</label>
                      <input type="text" class="form-control" id="pendidikan_terakhir" name="pendidikan_terakhir" value="{{$pegawai->pendidikan_terakhir}}" required>
                    </div>
                    <div class="col-md-3">
                      <label for="nama_instuisi" class="form-label fw-semibold">Nama Institusi</label>
                      <input type="text" class="form-control" id="nama_instuisi" name="nama_instuisi" value="{{$pegawai->nama_instuisi}}" required>
                    </div>
                    <div class="col-md-3">
                      <label for="tahun_lulus" class="form-label fw-semibold">Tahun Lulus</label>
                      <select class="form-control" id="tahun_lulus" name="tahun_lulus" required>
                        @for($year = date('Y'); $year >= 1980; $year--)
                          <option value="{{ $year }}" {{ $pegawai->tahun_lulus == $year ? 'selected' : '' }}>{{ $year }}</option>
                        @endfor
                      </select>
                    </div>
                    {{-- Baris 6 --}}
                    <div class="col-md-3">
                      <label for="status_pegawai" class="form-label fw-semibold">Status Pegawai</label>
                      <select class="form-control" id="status_pegawai" name="status_pegawai" required>
                        <option value="">Pilih Status Pegawai</option>
                        <option value="Tetap" {{ $pegawai->status_pegawai == 'Tetap' ? 'selected' : '' }}>Tetap</option>
                        <option value="Kontrak" {{ $pegawai->status_pegawai == 'Kontrak' ? 'selected' : '' }}>Kontrak</option>
                        <option value="Magang" {{ $pegawai->status_pegawai == 'Magang' ? 'selected' : '' }}>Magang</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label for="gaji" class="form-label fw-semibold">Gaji</label>
                      <input type="text" class="form-control" id="gaji" name="gaji" value="{{$pegawai->gaji}}" required>
                    </div>
                    <div class="col-md-6">
                      <label for="foto" class="form-label fw-semibold">Foto</label>
                      <input type="file" name="foto" id="foto" class="form-control shadow-none" accept="image/*">
                      <p>Foto saat ini :</p>
                      <img src="{{ asset('assets/img/upload/'.$pegawai->foto) }}" alt="foto saat ini" width="100px">
                    </div>
                    {{-- Button Submit --}}
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="submit">Submit</button>
                      <a href="{{ route('dashboard') }}" class="btn btn-secondary" type="submit">Kembali</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery-3.7.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>

  <!-- jQuery Scrollbar -->
  <script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
  <!-- Kaiadmin JS -->
  <script src="{{ asset('assets/js/kaiadmin.min.js') }}"></script>
  <!-- Kaiadmin DEMO methods, don't include it in your project! -->
  <script src="{{ asset('assets/js/setting-demo2.js') }}"></script>
  <!-- Fonts and icons -->
  <script src="{{ asset('assets/js/plugin/webfont/webfont.min.js') }}"></script>
  <script>
    WebFont.load({
      google: { families: ["Public Sans:300,400,500,600,700"] },
      custom: {
        families: [
          "Font Awesome 5 Solid",
          "Font Awesome 5 Regular",
          "Font Awesome 5 Brands",
          "simple-line-icons",
        ],
        urls: ["{{ asset('assets/css/fonts.min.css') }}"],
      },
      active: function () {
        sessionStorage.fonts = true;
      },
    });
  </script>
</body>

</html>
