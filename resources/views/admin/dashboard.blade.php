<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Dashboard | Pegawai</title>
  <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" />
  {{-- icon website --}}
  <link rel="icon" href="{{ asset('assets/img/kaiadmin/favicon.ico') }}" type="image/x-icon" />

  <!-- CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/plugins.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('assets/css/kaiadmin.min.css') }}" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />
</head>

<body>
  <div class="wrapper">
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- End Sidebar -->

    <div class="main-panel">
      {{-- Header --}}

      @include('admin.template.header')

      {{-- End Header --}}

      <div class="container">
        <div class="page-inner">
          <div class="page-header">
            <h3 class="fw-bold mb-3">Dashboard</h3>
            <ul class="breadcrumbs mb-3">
              <li class="nav-home">
                <a href="{{route('dashboard')}}">
                  <i class="icon-home"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    <a href="/tambah" class="btn btn-secondary">
                      <span class="btn-label">
                        <i class="fa fa-plus"></i>
                      </span>
                      Tambah Pegawai
                    </a>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table mt-3">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">Tanggal Bergabung</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    @php
                        $counter = 1;
                    @endphp
                    @foreach ($data as $item)
                      <tbody>
                        <tr>
                          <td>{{$counter++}}</td>
                          <td>{{ $item->nama }}</td>
                          <td>{{ $item->nama_jabatan }}</td>
                          <td>{{ $item->tanggal_bergabung }}</td>
                          <td>
                            <a class="btn btn-warning btn-sm" href="/admin/{{ $item->id }}/detail">
                              <i class="fas fa-eye"></i> Detail
                            </a>
                            <a class="btn btn-info btn-sm" href="/admin/{{ $item->id }}/edit">
                              <i class="icon-pencil"></i> Edit
                            </a>
                            <a class="btn btn-danger btn-sm" href="/admin/{{ $item->id }}/delete" onclick="return confirm('Apakah yakin ingin menghapus?')">
                                <i class="icon-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery-3.7.1.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>

  <!-- Sweet Alert -->
  <script src="{{ asset('assets/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

  <!-- jQuery Scrollbar -->
  <script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
  <!-- Kaiadmin JS -->
  <script src="{{ asset('assets/js/kaiadmin.min.js') }}"></script>
  <!-- Kaiadmin DEMO methods, don't include it in your project! -->
  <script src="{{ asset('assets/js/setting-demo2.js') }}"></script>
   <!-- Fonts and icons -->
  <script src="{{ asset('assets/js/plugin/webfont/webfont.min.js') }}"></script>
  <script>
    WebFont.load({
      google: { families: ["Public Sans:300,400,500,600,700"] },
      custom: {
        families: [
          "Font Awesome 5 Solid",
          "Font Awesome 5 Regular",
          "Font Awesome 5 Brands",
          "simple-line-icons",
        ],
        urls: ["{{ asset('assets/css/fonts.min.css') }}"],
      },
      active: function () {
        sessionStorage.fonts = true;
      },
    });
  </script>

  <!-- SweetAlert script to display session messages -->
  <script>
    $(document).ready(function() {
      @if (session()->has('tambah_data'))
        swal('{{ session('tambah_data') }}', {
          icon: "success",
          buttons: {
            confirm: {
              className: "btn btn-success",
            },
          },
        });
      @endif

      @if (session()->has('delete_data'))
        swal('{{ session('delete_data') }}', {
          icon: "success",
          buttons: {
            confirm: {
              className: "btn btn-success",
            },
          },
        });
      @endif

      @if (session()->has('edit_data'))
        swal('{{ session('edit_data') }}', {
          icon: "success",
          buttons: {
            confirm: {
              className: "btn btn-success",
            },
          },
        });
      @endif
    });
  </script>
</body>

</html>
