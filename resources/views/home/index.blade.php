<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    {{-- icon website --}}
    <link rel="icon" href="{{ asset('assets/img/kaiadmin/favicon.ico') }}" type="image/x-icon" />

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

</head>
<body>
    {{-- Navbar --}}
    <nav id="mainNavbar" class="navbar navbar-expand-lg shadow-sm bg-body-tertiary p-2">
        <div class="container d-flex align-items-center">
            <a class="navbar-brand fw-bold d-flex align-items-center" href="{{ route('index')}}">
                <img src="{{ asset('assets/img/kaiadmin/logo_light.svg') }}" alt="Logo" width="120" height="70"
                    class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link fw-medium text-white" aria-current="page" href="{{ route('index')}}">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-medium text-white" href="#scrollspyHeading1">Statistik</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-primary py-1 mx-1" href="{{ route('login')}}">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Content -->
    <section class="container-fluid p-0 m-0 scrollspy-example" data-bs-spy="scroll" data-bs-target="#mainNavbar" data-bs-root-margin="0px 0px -40%" data-bs-smooth-scroll="true" tabindex="0">
        {{-- Carousel --}}
        <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('assets/img/1.png') }}" class="d-block w-100 h-100" alt="tumpeng">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/img/2.png') }}" class="d-block w-100 h-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
    </section>

    <!-- Statistik Data Pegawai -->
    <div class="container-fluid py-3 bg-navy">
        <h1 class="fw-semibold text-center text-white mb-3" id="scrollspyHeading1">Statistik Data Pegawai</h1>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card animate__animated animate__fadeIn">
                        <div class="card-body text-center">
                            <h3 class="fw-semibold">Total Pegawai</h3>
                            <h1 class="fw-bold fs-3">{{ $totalPegawai }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card animate__animated animate__fadeIn">
                        <div class="card-body text-center">
                            <h3 class="fw-semibold">Pegawai Pria</h3>
                            <h1 class="fw-bold fs-3">{{ $totalPria }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card animate__animated animate__fadeIn">
                        <div class="card-body text-center">
                            <h3 class="fw-semibold">Pegawai Wanita</h3>
                            <h1 class="fw-bold fs-3">{{ $totalWanita }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <canvas id="pegawaiChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.0/gsap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            gsap.from("#mainNavbar", {
                duration: 1,
                opacity: 0,
                y: -50,
                ease: "power2.out"
            });
            gsap.from(".card", {
                duration: 1,
                opacity: 0,
                y: 50,
                stagger: 0.2
            });
        });

        $(document).ready(function() {
            var ctx = document.getElementById('pegawaiChart').getContext('2d');
            var pegawaiChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Pria', 'Wanita'],
                    datasets: [{
                        label: 'Jumlah Pegawai',
                        data: [{{ $totalPria }}, {{ $totalWanita }}],
                        backgroundColor: [
                            'rgba(23, 125, 255)',
                            'rgba(255, 99, 132)'
                        ],
                        borderColor: [
                            'rgba(54, 162, 235)',
                            'rgba(255, 99, 132)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        });
    </script>
</body>
</html>
