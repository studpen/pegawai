<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    {{-- icon website --}}
    <link rel="icon" href="{{ asset('assets/img/kaiadmin/favicon.ico') }}" type="image/x-icon" />

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />

</head>
<body>
    <section class="vh-100">
        <div class="container py-5 h-100">
          <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-8 col-lg-7 col-xl-6">
              <img src="{{ asset('assets/img/Civil_Worker_1.jpg') }}"
                class="img-fluid" alt="Phone image">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                <h1>Login</h1>
                @if (session('loginError'))
                    <div class="alert alert-danger">
                        {{ session('loginError') }}
                    </div>
                @endif
                <form action="{{ route('login.post') }}" method="POST" onSubmit="validasi()">
                    @csrf
                    <div class="form-floating mb-3">
                        <input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" value="{{ old('email') }}">
                        <label for="email">Email</label>
                        <div class="invalid-feedback">
                            
                        </div>
                    </div>
                    <div class="form-floating">
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                        <label for="password">Password</label>
                    </div>
                    <div class="d-flex justify-content-start">
                        <button type="submit" class="btn btn-primary btn-lg btn-block mt-3 me-1">Login</button>
                        <a href="{{route('index')}}" class="btn btn-outline-secondary border-0 btn-lg btn-block mt-3 ms-1">Kembali</a>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </section>
    
    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
    <script>
        function validasi() {
            var email = document.getElementById("email").value;
            var nama = document.getElementById("password").value;
		if (nama != "" && password!="") {
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}
    </script>
</body>
</html>
