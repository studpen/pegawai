<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->id();
            $table->string('NIK');
            $table->string('NPWP')->nullable();
            $table->string('nama');
            $table->enum('jenis_kelamin',['Laki-laki','Perempuan']);
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('no_telepon');
            $table->string('email');
            $table->string('nama_jabatan');
            $table->date('tanggal_bergabung');
            $table->string('pendidikan_terakhir');
            $table->string('nama_instuisi');
            $table->year('tahun_lulus');
            $table->string('status_pegawai');
            $table->integer('gaji');
            $table->string('foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pegawais');
    }
};
