<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('pegawais')->insert([
            [
                'NIK' => '3525021008010009', 
                'NPWP' => '12.345.678.9-012.000',
                'nama' => 'Bachtiar',
                'tanggal_lahir' => '2002-05-09',
                'alamat' => 'Bangkalan',
                'jenis_kelamin' => 'Laki-laki',
                'email' => 'bachtiar@gmail.com',
                'no_telepon' => '087678567234',
                'nama_jabatan' => 'General Manager',
                'tanggal_bergabung' => '2022-05-09',
                'pendidikan_terakhir' => 'S1 Teknik Informatika',
                'nama_instuisi' => 'UTM',
                'tahun_lulus' => '2021',
                'status_pegawai' => 'Tetap',
                'gaji' => '10000000',
                'foto' => 'profile.jpg',   
            ],
            [
                'NIK' => '3525021209030009', 
                'NPWP' => '12.345.678.9-012.000',
                'nama' => 'Nina',
                'tanggal_lahir' => '2002-05-09',
                'alamat' => 'Bangkalan',
                'jenis_kelamin' => 'Perempuan',
                'email' => 'nina@gmail.com',
                'no_telepon' => '087678567234',
                'nama_jabatan' => 'General Manager',
                'tanggal_bergabung' => '2022-05-09',
                'pendidikan_terakhir' => 'S1 Teknik Informatika',
                'nama_instuisi' => 'UTM',
                'tahun_lulus' => '2021',
                'status_pegawai' => 'Tetap',
                'gaji' => '10000000',
                'foto' => 'profile.jpg',   
            ],
            [
                'NIK' => '342502120803009', 
                'NPWP' => '12.345.678.9-012.000',
                'nama' => 'Supra',
                'tanggal_lahir' => '2002-05-09',
                'alamat' => 'Bangkalan',
                'jenis_kelamin' => 'Laki-laki',
                'email' => 'Supra@gmail.com',
                'no_telepon' => '087678567234',
                'nama_jabatan' => 'General Manager',                
                'tanggal_bergabung' => '2022-05-09',
                'pendidikan_terakhir' => 'S1 Teknik Informatika',
                'nama_instuisi' => 'UTM',
                'tahun_lulus' => '2021',
                'status_pegawai' => 'Tetap',
                'gaji' => '10000000',
                'foto' => 'profile.jpg',   
            ],
        ]);
    }
}
