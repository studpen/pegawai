<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function dashboard()
    {
        $data = Pegawai::select('*')->get();
        return view('admin/dashboard',compact('data'));
    }

    public function hal_tambah()
    {
        return view('admin/tambah', [
            'title' => 'Tambah Pegawai'
        ]);
    }

    public function tambah_data(Request $request)
    {
        $pegawai = Pegawai::create($request->except(['token', 'submit']));
        if($request->has(('foto'))){
            $request->file('foto')->move('assets/img/upload/', $request->file('foto')->getClientOriginalName());
            $pegawai->foto = $request->file('foto')->getClientOriginalName();
            $pegawai->save();
        }
        if ($pegawai->save()) {
            return redirect('/admin')->with('tambah_data', 'Data Pegawai Berhasil Ditambah!');
        };
    }

    public function hal_edit($id)
    {
        return view('admin/edit', [
            'title' => 'Edit Data',
            'pegawai'=> Pegawai::find($id),
        ]);
    }
    
    public function detail_pegawai($id)
    {
        return view('admin/detail', [
            'title' => 'Detail Pegawai',
            'pegawai'=> Pegawai::find($id),
        ]);
    }
    public function edit_data(Request $request)
    {
        $paket = Pegawai::find($request->id);
        $paket->NIK = $request->NIK;
        $paket->NPWP = $request->NPWP;
        $paket->nama = $request->nama;
        $paket->tanggal_lahir = $request->tanggal_lahir;
        $paket->jenis_kelamin = $request->jenis_kelamin;
        $paket->alamat = $request->alamat;
        $paket->no_telepon = $request->no_telepon;
        $paket->email = $request->email;
        $paket->nama_jabatan = $request->nama_jabatan;
        $paket->tanggal_bergabung = $request->tanggal_bergabung;
        $paket->pendidikan_terakhir = $request->pendidikan_terakhir;
        $paket->nama_instuisi = $request->nama_instuisi;
        $paket->tahun_lulus = $request->tahun_lulus;
        $paket->status_pegawai = $request->status_pegawai;

        $request->validate([
            'foto' => 'required|image|mimes:pjeg,png,jpg,gif,svg',
         ]);
        // Periksa apakah ada file foto yang diunggah
        if ($request->has('foto')) {
            // Hapus foto lama jika ada
            if ($paket->foto) {
                Storage::delete('assets/img/' . $paket->foto);
            }

            $request->file('foto')->move('assets/img/upload/', $request->file('foto')->getClientOriginalName());
            $paket->foto = $request->file('foto')->getClientOriginalName();
        }

        if ($paket->save()) {
            return redirect('/admin')->with("edit_data", "Berhasil Diupdate!");
        } else {
            // Handle the case where the save fails
            return redirect('/admin')->with("edit_data", "Gagal Diupdate!");
        }
    }

    public function delete_data($id)
    {
        Pegawai::find($id)->delete();
        return redirect()->back()->with("delete_data","Data Berhasil di Hapus");
    }
    
}
