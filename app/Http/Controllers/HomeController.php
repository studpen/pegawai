<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pegawai;

class HomeController extends Controller
{
    public function index()
    {
        $totalPegawai = Pegawai::count();
        $totalPria = Pegawai::where('jenis_kelamin', 'Laki-laki')->count();
        $totalWanita = Pegawai::where('jenis_kelamin', 'Perempuan')->count();

        return view('home.index', [
            'title' => 'Index',
            'totalPegawai' => $totalPegawai,
            'totalPria' => $totalPria,
            'totalWanita' => $totalWanita,
        ]);
    }
}
