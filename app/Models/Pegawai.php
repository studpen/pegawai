<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;
    protected $table = 'pegawais';
    protected $guards = [];
    protected $fillable=['NIK','NPWP','nama','tanggal_lahir','jenis_kelamin','alamat','no_telepon','email','nama_jabatan','tanggal_bergabung','status_pegawai','pendidikan_terakhir','nama_instuisi','tahun_lulus','gaji','foto'];
}
