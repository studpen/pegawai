<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [LoginController::class, 'login'])->name('login');
Route::post('/home', [LoginController::class, 'authenticate'])->name('login.post');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', [AdminController::class, 'dashboard'])->middleware('auth')->name('dashboard');
    // tambah data
    Route::get('/tambah', [AdminController::class, 'hal_tambah'])->name('hal_tambah');
    Route::post('/tambah_data', [AdminController::class,'tambah_data'])->name('tambah_data');
    // edit data
    Route::get('/admin/{id}/edit', [AdminController::class, 'hal_edit'])->name('hal_edit');
    Route::post('/admin/{id}/edit_data', [AdminController::class, 'edit_data'])->name('update_data');
    Route::get('/admin/{id}/delete', [AdminController::class, 'delete_data'])->name('delete_data');
    // detail
    Route::get('/admin/{id}/detail', [AdminController::class, 'detail_pegawai'])->name('detail_pegawai');


});